# Serverless AWS Node REST API with DynamoDB and offline support

This example expands upon the [Serverless example](https://github.com/serverless/examples/tree/master/aws-node-rest-api-with-dynamodb-and-offline) by creating the ability to create lists which organize your todo items.

## Setup

```bash
npm install
sls dynamodb install
sls offline start
sls dynamodb migrate (this imports schema)
```

## Run service offline

```bash
sls offline start
```

## Usage

You can create, retrieve, update, or delete todo lists and todos with the following commands:

### Create a Todo List

```bash
curl -X POST -H "Content-Type:application/json" http://localhost:3000/list --data '{ "text": "My very first todo list" }'
```

Example Result:
```bash
{"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","type":"list","text":"My very first todo list","created":"2018-06-24T18:40:02.041Z"}%
```

### List all Todo Lists

```bash
curl -H "Content-Type:application/json" http://localhost:3000/list
```

Example output:
```bash
[{"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","text":"My very first todo list","type":"list","created":"2018-06-24T18:40:02.041Z"}]%
```

### Get one Todo List

```bash
# Replace the <id> part with a real id from your todo list table
curl -H "Content-Type:application/json" http://localhost:3000/list/<id>
```

Example Result:
```bash
{"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","text":"My very first todo list","type":"list","created":"2018-06-24T18:40:02.041Z"}%
```

### Update a Todo List

```bash
# Replace the <id> part with a real id from your todo list table
curl -X PUT -H "Content-Type:application/json" http://localhost:3000/list/<id> --data '{ "text": "My very first todo list UPDATED!" }'
```

Example Result:
```bash
{"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","text":"My very first todo list UPDATED!","type":"list","updated":"2018-06-24T18:48:03.695Z","created":"2018-06-24T18:40:02.041Z"}%
```

### Delete a Todo List

```bash
# Replace the <id> part with a real id from your todo list table
curl -X DELETE -H "Content-Type:application/json" http://localhost:3000/todo/<id>
```

---

### Create a Todo

```bash
# Replace the <id> part with a real id from your todo list table
curl -X POST -H "Content-Type:application/json" http://localhost:3000/list/<id>/todo --data '{ "text": "My first todo" }'
```

Example Result:
```bash
{"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","type":"todo_b3caea0a-25eb-4785-ba84-ccbb2619337c","text":"My first todo","checked":false,"created":"2018-06-24T18:51:42.815Z"}%
```

### List all Todos

```bash
# Replace the <id> part with a real id from your todo list table
curl -H "Content-Type:application/json" http://localhost:3000/list/<id>/todo
```

Example output:
```bash
[{"checked":false,"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","text":"My first todo","type":"todo_b3caea0a-25eb-4785-ba84-ccbb2619337c","created":"2018-06-24T18:51:42.815Z"}]%
```

### Get one Todo

```bash
# Replace the <id> and <tid> part with a real id and tid from your todo list table
curl -H "Content-Type:application/json" http://localhost:3000/list/<id>/todo/<tid>
```

Example Result:
```bash
{"checked":false,"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","text":"My first todo","type":"todo_b3caea0a-25eb-4785-ba84-ccbb2619337c","created":"2018-06-24T18:51:42.815Z"}%
```

### Update a Todo

```bash
# Replace the <id> and <tid> part with a real id and tid from your todos table
curl -X PUT -H "Content-Type:application/json" http://localhost:3000/list/<id>/todo/<tid> --data '{ "text": "My very first todo item EDITED", "checked": true }'
```

Example Result:
```bash
{"checked":true,"id":"a3d0f4fe-5193-4721-aaea-11a0f80bc04c","text":"My very first todo item EDITED","type":"todo_b3caea0a-25eb-4785-ba84-ccbb2619337c","updated":"2018-06-24T19:00:38.685Z","created":"2018-06-24T18:51:42.815Z"}%
```

### Delete a Todo

```bash
# Replace the <id> and <tid> part with a real id and tid from your todos table
curl -X DELETE -H "Content-Type:application/json" http://localhost:3000/list/<id>/todo<tid>
```

## Note
There isn't any permission checking. Most notably; creating a new todo item with a list id that doesn't exists returns a 200 OK response (but nothing will be created).