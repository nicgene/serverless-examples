'use strict';

const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const listTodoList = (event, context, callback) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    ExpressionAttributeNames: {
      '#type': 'type',
    },
    ExpressionAttributeValues: {
      ':type': 'list',
    },
    FilterExpression: '#type = :type',
  };

  dynamodb.scan(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: 'Couldn\'t fetch the todo list.',
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error fetching todo lists. Please try again.'
        }]})
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Items),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(listTodoList)
  .use(cors());