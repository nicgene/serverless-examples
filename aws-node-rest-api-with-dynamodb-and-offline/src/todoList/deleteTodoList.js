'use strict';

const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const deleteTodoList = (event, context, callback) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: event.pathParameters.id,
      type: 'list',
    },
  };

  dynamodb.delete(params, (error) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error deleting todo list. Please try again.'
        }]})
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify({}),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(deleteTodoList)
  .use(cors());