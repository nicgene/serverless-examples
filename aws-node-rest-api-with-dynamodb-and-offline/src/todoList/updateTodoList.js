'use strict';

const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const updateTodoList = (event, context, callback) => {
  const data = JSON.parse(event.body);

  // validation
  if (typeof data.text !== 'string' || data.text.length < 1 || data.text.length > 60) {
    console.error('Validation Failed');
    callback(null, {
      statusCode: 400,
      body: JSON.stringify({ 'errors': [{
        'title': 'Couldn\'t update the todo list.',
        'detail': 'Text must be a string less than 60 characters.'
      }]}),
    });
    return;
  }

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: event.pathParameters.id,
      type: 'list',
    },
    ExpressionAttributeNames: {
      '#text': 'text',
      '#updated': 'updated',
    },
    ExpressionAttributeValues: {
      ':text': data.text,
      ':updated': new Date().toISOString(),
    },
    UpdateExpression: 'SET #text = :text, #updated = :updated',
    ReturnValues: 'ALL_NEW',
  };

  dynamodb.update(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error updating todo list. Please try again.'
        }]})
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify(result.Attributes),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(updateTodoList)
  .use(cors());