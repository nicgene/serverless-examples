'use strict';

const { cors } = require('middy/middlewares');

module.exports = (options) => cors({
  origins: ['http://localhost:8080'],
  options
});