'use strict';

const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const deleteTodoItem = (event, context, callback) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: event.pathParameters.id,
      type: 'todo_' + event.pathParameters.todoId,
    },
  };

  dynamodb.delete(params, (error) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error deleting todo item. Please try again.'
        }]})
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify({}),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(deleteTodoItem)
  .use(cors());