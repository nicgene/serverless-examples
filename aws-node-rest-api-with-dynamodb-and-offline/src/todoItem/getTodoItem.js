'use strict';

const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const getTodoItem = (event, context, callback) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: event.pathParameters.id,
      type: 'todo_' + event.pathParameters.todoId,
    },
  };

  dynamodb.get(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error fetching todo item. Please try again.'
        }]})
      });
      return;
    }

    const item = result.Item;
    item.type = item.type.split('_')[1];

    const response = {
      statusCode: 200,
      body: JSON.stringify(item),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(getTodoItem)
  .use(cors());