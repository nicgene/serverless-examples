'use strict';

const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const listTodoItem = (event, context, callback) => {
  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    ExpressionAttributeNames: {
      '#id': 'id',
    },
    ExpressionAttributeValues: {
      ':id': event.pathParameters.id,
    },
    FilterExpression: '#id = :id',
  };

  dynamodb.scan(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error fetching todo items. Please try again.'
        }]})
      });
      return;
    }

    const items = [];
    for (var i = 0; i < result.Items.length; i++) {
      var item = result.Items[i];
      var itemType = item.type.split('_');
      if (itemType[0] != 'list') {
        item.type = itemType[1];
      }
      items.push(item);
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify(items),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(listTodoItem)
  .use(cors());