'use strict';

const uuid = require('uuid');
const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const createTodoItem = (event, context, callback) => {
  const data = JSON.parse(event.body);
  if (typeof data.text !== 'string' || data.text.length < 1 || data.text.length > 60) {
    console.error('Validation Failed');
    callback(null, {
      statusCode: 400,
      body: JSON.stringify({ 'errors': [{
        'title': 'Couldn\'t create the todo item.',
        'detail': 'Text must be a string less than 60 characters.'
      }]})
    });
    return;
  }

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Item: {
      id: event.pathParameters.id,
      type: 'todo_' + uuid.v4(),
      text: data.text,
      checked: false,
      created: new Date().toISOString(),
    },
  };

  dynamodb.put(params, (error) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error saving todo item. Please try again.'
        }]})
      });
      return;
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify(params.Item),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(createTodoItem)
  .use(cors());
