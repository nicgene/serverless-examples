'use strict';

const dynamodb = require('../dynamodb');
const middy = require('middy');
const cors = require('../cors');

const updateTodoItem = (event, context, callback) => {
  const data = JSON.parse(event.body);

  if (typeof data.text !== 'string' || data.text.length < 1 || data.text.length > 60 || typeof data.checked !== 'boolean') {
    console.error('Validation Failed');
    callback(null, {
      statusCode: 400,
      body: JSON.stringify({ 'errors': [{
        'title': 'Couldn\'t update the todo item.',
        'detail': 'Text must be a string less than 60 characters.'
      }]}),
    });
    return;
  }

  const params = {
    TableName: process.env.DYNAMODB_TABLE,
    Key: {
      id: event.pathParameters.id,
      type: 'todo_' + event.pathParameters.todoId,
    },
    ExpressionAttributeNames: {
      '#text': 'text',
      '#checked': 'checked',
      '#updated': 'updated',
    },
    ExpressionAttributeValues: {
      ':text': data.text,
      ':checked': data.checked,
      ':updated': new Date().toISOString(),
    },
    UpdateExpression: 'SET #text = :text, #checked = :checked, #updated = :updated',
    ReturnValues: 'ALL_NEW',
  };

  dynamodb.update(params, (error, result) => {
    if (error) {
      console.error(error);
      callback(null, {
        statusCode: error.statusCode || 501,
        body: JSON.stringify({ 'errors': [{
          'title': 'Internal server error.',
          'detail': 'Error updating todo item. Please try again.'
        }]})
      });
      return;
    }

    const item = result.Attributes;
    item.type = item.type.split('_')[1];

    const response = {
      statusCode: 200,
      body: JSON.stringify(item),
    };
    callback(null, response);
  });
};

module.exports.handler = middy(updateTodoItem)
  .use(cors());