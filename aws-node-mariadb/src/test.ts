import * as mariadb from 'mariadb';

export const handler = async () => {
  let conn = await mariadb.createConnection({host: process.env.MARIADB_HOST, user: process.env.MARIADB_USER, password: process.env.MARIADB_PASSWORD});
  try {
    let rows = await conn.query("SELECT 1 as val");
    console.log(rows);
    await conn.end();
  } catch (error) {
    console.error(error);
  }

  return {
    statusCode: 200,
    body: JSON.stringify({ message: 'dev/null.' })
  }
}