'use strict';

module.exports.test = (event, context, callback) => {
  const name = event.pathParameters.name
  
  const response = {
    statusCode: 200,
    body: JSON.stringify({ name: name })
  };

  callback(null, response);
}