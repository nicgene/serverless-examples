# Dockerize Your Serverless Development Environment

Deploying software as microservices is great but without matching our local server stack to the one in staging or production we can't accurately test our software. Solutions such as Node Version Manager is one way to ensure we are running the same version of Node as that in production. However we can also containerize our development environment and along with it store project specific credentials to make deployment easier.

## Run service offline

```bash
docker-compose up
```

## Re-build the image
```bash
docker-compose up --build
```

## Usage

Access the test function: [http://localhost:49160/dupkey](http://localhost:49160/dupkey)

You can also build and run the image via cli:

docker build -t serverless/docker-serverless-development-environment .

docker run -p 49160:3000 serverless/docker-serverless-development-environment

Note: If you should have a need to deploy the image you can uncomment the additional commands in the Dockerfile.

## Execute commands on the running container

docker exec -it <container_id_or_name> sls config credentials --provider aws --key <KEY> --secret <SECRET>

### Check out the Serverless docs for more commands

https://serverless.com/framework/docs/providers/aws/guide/credentials/