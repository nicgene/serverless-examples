'use strict';

const { GraphQLServerLambda } = require('graphql-yoga')
const mongoose = require('mongoose')
const resolvers = require('./resolvers')

mongoose.connect(process.env.MONGO_HOST)
const db = mongoose.connection
db.on('error', console.error.bind(console, 'Connection Error:'))
db.once('open', function() {
  console.log("Connection Succeeded")
});

const server = new GraphQLServerLambda({
  typeDefs: 'src/schema.graphql',
  resolvers,
  // context: req => req
  context: ({ event }) => event
});

module.exports.server = server.graphqlHandler
module.exports.playground = server.playgroundHandler