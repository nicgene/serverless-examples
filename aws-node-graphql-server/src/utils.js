const jwt = require('jsonwebtoken')

function getUserId(context) {
  const authorization = context.headers.authorization

  // if (typeof authorization != 'undefined') {
    if (typeof authorization != undefined) {
    const token = authorization.replace('Bearer ', '')
    const {id} = jwt.verify(token, process.env.JWT_SECRET)
    
    return id
  }

  throw new Error('Not authenticated')
}

module.exports = {
  getUserId,
}