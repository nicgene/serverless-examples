# Serverless AWS Node GraphQL Server

These serverless functions create a graphql server using express and graphql-yoga. The server handler is where you can directly send GraphQL queries and the playground handler is the interactive website for testing. The graphql-yoga packagae uses GraphQL server and is built on Apache Apollo.

This example is adapted from Kenzo Takahashi's Medium artcle [Build a Project Management Tool with Vue.js, Node.js and Apollo](https://itnext.io/build-a-project-management-software-with-vue-js-and-apollo-part1-d12ee75a7641). Updated to step 5!

If you don't have MongoDB installed, download it and start the process. In serverless.yml update the MongoDB host address if it differs.

## Setup

```bash
yarn
```

## Run service offline

```bash
sls offline -c
```

The -c flag tells the serverless offline plugin to skip require cache invalidation. This is nessessary to avoid a MongoDB error 'Cannot overwrite model once compiled'.

## Usage

Use the GraphQL Playground: [http://localhost:3000/](http://localhost:3000/)